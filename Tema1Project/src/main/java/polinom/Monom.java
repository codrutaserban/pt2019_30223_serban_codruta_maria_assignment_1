package polinom;

public class Monom implements Comparable<Monom> {
		private int grad;
		private double coeficient;
		
		Monom(int grad, double coef){
			this.grad=grad;
			this.coeficient=coef;
		}
		Monom(){
			this.grad=0;
			this.coeficient=0;
		}
		
		public String toString() {
			String s=new String();
			if(this.coeficient==1 || this.coeficient==-1) {
				switch(this.grad) {
				case 0 : s= (this.coeficient==1) ? "1" : "-1";break; // monoame de forma 1,-1
				case 1 : s=(this.coeficient==1) ? s+'x' : s+"-x";break;// monoame de forma x, -x
				default :s=(this.coeficient==1) ? s+"x^"+this.grad : s+"-x^"+this.grad;break;//monoame de forma x^2, -x^8
				}
			}
			else {
				if(this.grad==0) {
					s=((int) this.coeficient==this.coeficient) ? s+(int)this.coeficient:s+String.format("%.2f",this.coeficient);// monoame de forma 5x
				}
				else if(this.grad==1) {
					s=((int) this.coeficient==this.coeficient) ? s+(int)this.coeficient+"x":s+String.format("%.2f",this.coeficient)+"x";// monoame de forma 5.33x
				}
				else {
					if((int) this.coeficient==this.coeficient) {
						s=s+(int)this.coeficient+"x^"+Integer.toString(this.grad);// monoame de forma 5x^2
					}
					else{
						s=s+String.format("%.2f",this.coeficient)+"x^"+Integer.toString(this.grad);//monoame 5x33^2
					}
				}
			}
			return s;
		}
		//compararea pentru a ordona sirul de monoame descrescator
		public int compareTo(Monom m1) {
			if(this.grad==m1.getGrad())return 0;
			else if(this.grad<m1.getGrad()) return 1;
			else return -1;
		}
		
		public int getGrad() {
			return grad;
		}
		public void setGrad(int grad) {
			this.grad = grad;
		}
		public double getCoeficient() {
			return this.coeficient;
		}
		public void setCoeficient(double coeficient) {
			this.coeficient = coeficient;
		}
}
