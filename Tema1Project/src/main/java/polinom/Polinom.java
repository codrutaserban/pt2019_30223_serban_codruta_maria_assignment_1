package polinom;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom {
      ArrayList <Monom> p= new ArrayList<Monom>(); 
      
      Polinom(int z){
    	  p.add(new Monom(z,z));
      }
      
      public Polinom(){
    	  
      }
      
      public Polinom adunare (Polinom p1){//p+p1
    	  Polinom rez= new Polinom();
    	  int i=0, j=0,k;
    	  while(i<p.size() && j<p1.getP().size()){//se adauga monomul care are gradul mai mare 
    		  if(p.get(i).getGrad()<p1.getP().get(j).getGrad()){
    			  if(p1.getP().get(j).getGrad()!=0) {
    			  rez.getP().add(new Monom(p1.getP().get(j).getGrad(),p1.getP().get(j++).getCoeficient()));
    			}   }
    		  else  if(p.get(i).getGrad()>p1.getP().get(j).getGrad()){
    			  if(p.get(i).getGrad()!=0) {
    				  rez.getP().add(new Monom(p.get(i).getGrad(),p.get(i++).getCoeficient()));
    			  }
    		  }
    		  else if(p.get(i).getGrad()==p1.getP().get(j).getGrad()){
    			 Monom m=new Monom();
    			 m.setGrad(p.get(i).getGrad());
    			 m.setCoeficient(p.get(i++).getCoeficient()+p1.getP().get(j++).getCoeficient());
    			 if(m.getCoeficient()!=0){ 
    				 rez.getP().add(m);}
    		  }
    	  }
    	  for( k=i;k<p.size() && p.size()>0 && p.get(0).getCoeficient()!=0;k++){//in cazul in care in p au ramas monoame acestea se adauga, cu exceptia monomului nul
    		  rez.getP().add(new Monom(p.get(i).getGrad(),p.get(i++).getCoeficient()));
    	  }
    	  for( k=j;k<p1.getP().size() && p1.getP().size()>0 && p1.getP().get(0).getCoeficient()!=0;k++){//in cazul in care in p1 au ramas monoame acestea se adauga, cu exceptia monomului nul
    		  rez.getP().add(new Monom(p1.getP().get(j).getGrad(),p1.getP().get(j++).getCoeficient()));
    	  }
    	  if(rez.getP().isEmpty()) {//daca rezultatul nu contine niciun monom se returneaza monomul nul
    		  rez.getP().add(new Monom(0,0));}
    	  return rez;
      }
      
      public Polinom scadere (Polinom p1){//p-p1 
    	  Polinom rez= new Polinom();
    	  int i=0, j=0,k;
    	  while(i<p.size() && j<p1.getP().size()){
    		  if(p.get(i).getGrad()<p1.getP().get(j).getGrad()){//se adauga monomul cu grad mai mare, iar daca el provine din p1 coeficientul ii este negat
    			  rez.getP().add(new Monom(p1.getP().get(j).getGrad(),p1.getP().get(j++).getCoeficient()*(-1)));
    		  }
    		  
    		  else if(p.get(i).getGrad()>p1.getP().get(j).getGrad()){
    			 rez.getP().add(new Monom(p.get(i).getGrad(),p.get(i++).getCoeficient()));
    		  }
    		  else if(p.get(i).getGrad()==p1.getP().get(j).getGrad()){
    			 Monom m=new Monom();
    			 m.setGrad(p.get(i).getGrad());
    			 m.setCoeficient(p.get(i++).getCoeficient()-p1.getP().get(j++).getCoeficient());
    			 if(m.getCoeficient()!=0){ 
    				 rez.getP().add(m);
    			 }
    		  }
    	  }
    	  for( k=i;k<p.size() && p.size()>0 && p.get(0).getCoeficient()!=0;k++){//in cazul in care in p au ramas monoame acestea se adauga, cu exceptia monomului nul
    		  rez.getP().add(new Monom(p.get(i).getGrad(),p.get(i++).getCoeficient()));
    	  }
    	  for( k=j;k<p1.getP().size() && p1.getP().size()>0 && p1.getP().get(0).getCoeficient()!=0;k++){//in cazul in care in p au ramas monoame acestea se scad, cu exceptia monomului nul
    		  rez.getP().add(new Monom(p1.getP().get(j).getGrad(),p1.getP().get(j++).getCoeficient()*(-1)));
    	  }
    	  if(rez.getP().isEmpty()) {//daca rezultatul nu contine niciun monom se returneaza monomul nul
    		  rez.getP().add(new Monom(0,0));}
    	  return rez;
      }
      
      public Polinom inmultire (Polinom p1){//p*p1
    	  Polinom rez= new Polinom();
    	  double c=0;
    	  if(p.get(0).getCoeficient()==0 || p1.getP().get(0).getCoeficient()==0) {
    		  rez.getP().add(new Monom(0,0));// daca unul dintre polinoame este nul se returneaza direct 0
    		  return rez;
    	  }
    	  for( int k=0;k<(p.get(0).getGrad()+p1.getP().get(0).getGrad()+1);k++){
    		  rez.getP().add(new Monom());// initializam rez cu monoame nule intr-un numar egal cu suma gradelor maxime din cele 2 polinoame
    	  }
    	  for(Monom m:p) {
    	     for(Monom m1:p1.getP()) {  //se inmultesc monoamele si rezultatul e adaugat in rez la elementul cu indicele egal cu suma coeficientilor monoamelor
    			c=rez.getP().get( (int) (m.getGrad()+m1.getGrad())).getCoeficient();
    			if(c==0) {
    				rez.getP().get((int) (m.getGrad()+m1.getGrad())).setGrad(m.getGrad()+m1.getGrad());
    				rez.getP().get((int) (m.getGrad()+m1.getGrad())).setCoeficient(m.getCoeficient()*m1.getCoeficient());
    			}
    			else {//daca deja s-a introdus un monom in rez pe pozitia obtinuta produsul coeficientilor va fi adunat la valarea deja existenta a coeficientului din rez
    				rez.getP().get((int) (m.getGrad()+m1.getGrad())).setCoeficient(m.getCoeficient()*m1.getCoeficient()+c);
    			}
    		  }
    	  }
    		  rez.sortare(); 	  
    	  for(int i=0;i<rez.getP().size();i++){
    		  if(rez.getP().get(i).getCoeficient()==0) {//elimi din rez monoamele care au coeficientul zero
    			  rez.getP().remove(rez.getP().get(i--));
    		  }
    	  }
    	  return rez;
      }
      
      public ArrayList<Polinom> impartire(Polinom p1) {// p/p1
    	  Polinom cat= new Polinom();
    	  Polinom aux= new Polinom(); 
    	  Polinom rest=new Polinom();
    	  ArrayList<Polinom> imp=new ArrayList<Polinom>();
    	  aux.getP().add(new Monom());
    	  if(p.get(0).getCoeficient()==0 ) {
    		  imp.add(new Polinom(0));
    		  imp.add(new Polinom(0));
    		  return imp;
    	  }
    	  for(Monom m:p) {
    		  rest.getP().add(new Monom(m.getGrad(),m.getCoeficient()));//initial rest= deimpartir
    	  };
    	  if(p.get(0).getGrad()< p1.getP().get(0).getGrad()) {
    		  cat.getP().add(new Monom(0,0));//daca gradul deimpartitului este mai mic decat cel al impartitorului catul este egal cu 0 iar restul este egal cu deimpartitorul
    	  }
    	  else while(rest.getP().get(0).getGrad()>= p1.getP().get(0).getGrad()) {//restul se imparte la primul monom din impartitor, monomul obtinut=aux este inmultit cu impartitorul iar rezultatul se scade din rest
    		  cat.getP().add(new Monom(rest.getP().get(0).getGrad()-p1.getP().get(0).getGrad(),rest.getP().get(0).getCoeficient()/p1.getP().get(0).getCoeficient()));
    		  aux.getP().get(0).setCoeficient(cat.getP().get(cat.getP().size()-1).getCoeficient());  
    		  aux.getP().get(0).setGrad(cat.getP().get(cat.getP().size()-1).getGrad());
    		  rest=rest.scadere(aux.inmultire(p1));
    		  aux.sortare();
    		  if(p1.getP().get(0).getGrad()==0) {
    			  break;//se repeta proderura pana gradul la rest >grad impartitor sau daca gradul la rest ajune sa fie 0
    		   }
    	      }
    	  imp.add(cat);
		  imp.add(rest);
		  return imp;
      }
      //se deriveaza fiecare monom din polinom conform principiului matematic
      public Polinom derivare(Polinom p1) {//  dp1/dx
    	  Polinom rez= new Polinom();
    	  for(Monom m:p1.getP()) {
    		  if(m.getGrad()!=0) {
    			  rez.getP().add(new Monom(m.getGrad()-1,m.getCoeficient()*m.getGrad()));
    		  }
    	  };
    	  if(rez.getP().isEmpty()) {
    		  rez.getP().add(new Monom(0,0));}
    	  return rez;
      }
      //se integreaza fiecare monom din polinom conform principiului matematic
      public Polinom integrare(Polinom p1) {//Sx
    	  Polinom rez= new Polinom();
    	  for(Monom m:p1.getP()) {
    		  if(m.getCoeficient()!=0) {
    		  rez.getP().add(new Monom(m.getGrad()+1,m.getCoeficient()/(m.getGrad()+1)));
    		  }
    	  };
    	  if(rez.getP().isEmpty()) {
    		  rez.getP().add(new Monom(0,0));}
    	  return rez;
      }
      
      // stringul introdus de utilizator este convertit intr-un polinom
	public Polinom transformare (String s)
      {
    	  Polinom rez= new Polinom();
    	  if(s.equals("0")) {
    		  rez.getP().add(new Monom(0,0));
    		  return rez;
    	  }
    	  Pattern pattern = Pattern.compile("([+-]?[^-+]+)");//acest pattern indica faptul ca ininte de un caracter ? mai pot sau nu exista alte caractere si la fel se intampla si cu ^, de asemenea x si ^pot lipsi
    	  Matcher matcher = pattern.matcher(s);//va contine grupurile de stringuri identificate dupa sablonul dat
    	  String g= new String();
    	  String c= new String();
    	  int indX,indp;//indicele caracterului x si al caracterului ^
    	  while (matcher.find()) {//obtnem gradul si coeficientul monomului in functie de pozitia din sir a caracterelor x si ^
    	      indX=matcher.group(1).indexOf('x');
    	      indp=matcher.group(1).indexOf('^');
    	      if(indX==-1) {
    	    	  g=Integer.toString(0);
    	    	  c=matcher.group(0);
    	      }
    	      else if(indX==0) {
    	    	  c=Integer.toString(1);
    	    	  g= (indp==-1) ? Integer.toString(1) : matcher.group(0).substring(indp+1);
    	      }
    	      else {
    	    	  c=matcher.group(0).substring(0,indX);
    	    	  g= (indp==-1) ? Integer.toString(1) :matcher.group(0).substring(indp+1);
    	      }
    	      if(c.compareTo("+")==0 || c.compareTo("-")==0) {
    	    	  c=c+'1';
    	      }
    	      if(g.compareTo("0")!=0 || c.compareTo("0")!=0) {
    	    	  rez.getP().add(new Monom(Integer.parseInt(g),Double.parseDouble(c)));
    	      }
    	  }
    	  if(rez.getP().isEmpty()) {
    	  rez.getP().add(new Monom(0,0));
    	  }
    	  return rez;
      }
      
       public void stgZero() {
    	int i;
    	String s;
    	for(i=0;i<p.size();i++)
    	{	s=String.format("%.2f",p.get(i).getCoeficient());
    		if(s.compareTo("0.00")==0) {
    			p.remove(p.get(i--));
    		}
    	}
    }
      
      public String toString() {
    	  String s=new String();
    	  s="";
    	for(Monom m:p)  {
    		  if(m.equals(p.get(0)) || m.getCoeficient()<0) {
    			  s=s+m.toString();
    			  }
    		  else {
    			  s=s+"+"+m.toString();
    		  }
  	    } 
    	  return s;
      }
      
   
      
    public void sortare() {
    	Collections.sort(this.p);
    }
	public ArrayList<Monom> getP() {
		return p;
	}
	public void setP(ArrayList<Monom> p) {
		this.p = p;
	}
}
