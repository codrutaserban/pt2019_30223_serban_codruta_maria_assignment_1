package controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import polinom.Polinom;
import view.Gui;

public class Controller {
	
	private Polinom p;
	private Gui g;
	private String p1;
	private String p2;
	private String operatie;
	
	public Controller(Polinom pol, Gui gr)
	{
		this.p=pol;
		this.g=gr;
		gr.addP1Listener(new citireP1());
		gr.addP2Listener(new citireP2());
		gr.addBtListener(new citireOp());
		gr.addButonListener(new detectareButon() );

	}

	// definim clasele care implementeaza interfata ActionListener acestea sunt folosite ca parametri la metodele de adaugare de ActionListener
	private class citireP1 implements ActionListener {
		public void actionPerformed(ActionEvent e){
				p1=g.getStringP1();
		}
	}
	
	private class citireP2 implements ActionListener {
		public void actionPerformed(ActionEvent e){
				p2=g.getStringP2();
		}
	}
	
	private class citireOp implements ActionListener {
		public void actionPerformed(ActionEvent e){
				operatie=g.getOperatie();
		}
	}
	//in functie de optiunea aleasa in ComboBox se va efectua operatia respectiva
		// in aceasta clasa se preiau sirurile de caractere introduse de utilizator, sunt convertite in polinoame, ulterior
		//se efectueaza operatia dorita si rezultatul va fi scris in JTextField din interfata pentru Rezultat si/sau Rest
		//dupa obtinerea polinoamelor din strinuri acestea se sorteaza in cazul in care au fost introduse in alta ordine
		//in campul Rest se vor afisa date doar in cazul impartirii
		//pentru fiecare operate daca nu au fost introduse polinoamele cu care urmeaza sa se lucreze se va deschide o fereasta de mesaj care indica
		//utilizatorului cum ar trebui sa foloseasca aplicatia
	private class detectareButon implements ActionListener {
		public void actionPerformed(ActionEvent e){
			Polinom pol1=new Polinom();
			Polinom pol2=new Polinom();
			Polinom rez=new Polinom();
			Polinom rest=new Polinom();
			ArrayList<Polinom> imp=new ArrayList<Polinom>();
			if(operatie.equals("Adunare")) {
				rez.getP().clear();
				p1=g.getStringP1();
				p2=g.getStringP2();
				if(p1.equals("--") || p2.equals("--")) {
					JOptionPane.showMessageDialog(null, "Introduceti ambele polinoame pentru adunare!");
				}
				else {
					pol1=rez.transformare(p1);
					System.out.println("1");
					pol2=rez.transformare(p2);System.out.println("2");
					pol1.sortare();
					pol2.sortare();
					rez=pol1.adunare(pol2);
					g.setStringRasp(rez.toString());
				}
				rez.stgZero();
				g.setStringRest("");
			}
			else if(operatie.equals("Scadere")) {
				rez.getP().clear();
				p1=g.getStringP1();
				p2=g.getStringP2();
				if(p1.equals("--") || p2.equals("--")) {
					JOptionPane.showMessageDialog(null, "Introduceti ambele polinoame pentru scadere!");
				}
				else {
					pol1=rez.transformare(p1);
					pol2=rez.transformare(p2);
					pol1.sortare();
					pol2.sortare();
					rez=pol1.scadere(pol2);
					g.setStringRasp(rez.toString());
				}
				g.setStringRest("");
			 }
			else if(operatie.equals("Inmultire")) {
				rez.getP().clear();
				p1=g.getStringP1();
				p2=g.getStringP2();
				if(p1.equals("--") || p2.equals("--")) {
					JOptionPane.showMessageDialog(null, "Introduceti ambele polinoame pentru inmultire!");
				}
				else {
					pol1=rez.transformare(p1);
					pol2=rez.transformare(p2);
					pol1.sortare();
					pol2.sortare();
					rez=pol1.inmultire(pol2);
					g.setStringRasp(rez.toString());
				}
				g.setStringRest("");
			}
			else if(operatie.equals("Impartire P1/P2")) {
				rez.getP().clear();
				p1=g.getStringP1();
				p2=g.getStringP2();
				if(p1.equals("--") || p2.equals("--")) {
					JOptionPane.showMessageDialog(null, "Introduceti ambele polinoame pentru impartire!");
				}
				else {
					if(p2.equals("0")) {
						JOptionPane.showMessageDialog(null, "Impartirea cu zero nu se poate efectua, intoduceti alt polinom 2!");
					}
					else {
					pol1=rez.transformare(p1);
					pol2=rez.transformare(p2);
					pol1.sortare();
					pol2.sortare();
					imp=pol1.impartire(pol2);
					rez=imp.get(0);
					rest=imp.get(1);
					g.setStringRasp(rez.toString());
					g.setStringRest(rest.toString());
					}
				}
			}
			else if(operatie.equals("Derivare P1")) {
				rez.getP().clear();
				p1=g.getStringP1();
				if(p1.equals("--")) {
					JOptionPane.showMessageDialog(null, "Introduceti primul polinom pentru derivare!");
				}
				else {
					pol1=rez.transformare(p1);
					pol1.sortare();
					rez=pol1.derivare(pol1);
					g.setStringRasp(rez.toString());
				}
				g.setStringRest("");
			}
			else if(operatie.equals("Derivare P2")) {
				rez.getP().clear();
				p2=g.getStringP2();
				if(p2.equals("--")) {
					JOptionPane.showMessageDialog(null, "Introduceti al doilea polinom pentru derivare!");
				}
				else {
					pol2=rez.transformare(p2);
					pol2.sortare();
					rez=pol2.derivare(pol2);
					g.setStringRasp(rez.toString());
				}
				g.setStringRest("");
			}
			else if(operatie.equals("Integrare P1")) {
				rez.getP().clear();
				p1=g.getStringP1();
				if(p1.equals("--")) {
					JOptionPane.showMessageDialog(null, "Introduceti primul polinom pentru integrare!");
				}
				else {
					pol1=rez.transformare(p1);
					rez=pol1.integrare(pol1);
					g.setStringRasp(rez.toString());
				}
				g.setStringRest("");
			}
			else if(operatie.equals("Integrare P2")) {
				rez.getP().clear();
				p2=g.getStringP2();
				if(p2.equals("--")) {
					JOptionPane.showMessageDialog(null, "Introduceti al doilea polinom pentru integrare!");
				}
				else {
					pol2=rez.transformare(p2);
					rez=pol2.integrare(pol2);
					g.setStringRasp(rez.toString());
				}
				g.setStringRest("");
			}
		}
	}
}
