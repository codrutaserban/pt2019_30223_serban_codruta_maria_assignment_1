import java.util.ArrayList;
import java.util.HashMap;

import controller.Controller;
import polinom.Polinom;
import view.Gui;

public class Main {

	public static void main(String[] args) {
		Gui g = new Gui();		
		Polinom pq=new Polinom();
		Controller c= new Controller(pq,g);
	}

}