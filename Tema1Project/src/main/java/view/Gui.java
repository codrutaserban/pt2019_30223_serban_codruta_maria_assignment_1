package view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
public class Gui{
	private	JLabel l1 ;
	private JTextField t1 ;
	private JLabel l2 ;
	private JTextField t2 ;
	private JComboBox cb ;
	private JButton bt; 
	private JLabel rasp;
	private JLabel rest ;
	private JTextField r1;
	private JTextField r2;
	
	
	//frameul contine un panel de tipul BoxLayout orientat pe axa Y. Acesta contine la randul sau inca 6 paneluri de acelasi tip insa orientate dupa asa X
	public Gui()
	{   l1 = new JLabel("P1:", JLabel.RIGHT);
		l2 = new JLabel("P2:", JLabel.RIGHT);
		t1 = new JTextField("--",2);
		t2 = new JTextField("--",2);
		cb = new JComboBox(new String[]{"Alegeti o operatie","Adunare", "Scadere", "Inmultire", "Impartire P1/P2", "Derivare P1","Derivare P2", "Integrare P1" ,"Integrare P2"});
		bt = new JButton("Egal"); 
		rasp = new JLabel("Raspuns:", JLabel.LEFT);
		rest = new JLabel("Rest:", JLabel.RIGHT);
		r1 = new JTextField("0",2);
		r2 = new JTextField("0",2);
		JFrame frame = new JFrame("Procesarea polinoamelor");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1000, 500);
		
		JPanel panel= new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	    
	    JPanel pol1 = new JPanel();
	    pol1.setLayout(new BoxLayout(pol1, BoxLayout.X_AXIS)); 
	    
	    JPanel pol2 = new JPanel();
	    pol2.setLayout(new BoxLayout(pol2, BoxLayout.X_AXIS)); 
	    
		
	    panel.add( Box.createRigidArea(new Dimension(40,0)) );

		l1.setFont(new Font("Arial",Font.PLAIN,25));
		
		t1.setFont(new Font("Arial",Font.PLAIN,25));
		pol1.add( Box.createRigidArea(new Dimension(20,0)) );
		pol1.add( l1 );
		pol1.add( Box.createRigidArea(new Dimension(20,0)) );
		pol1.add(t1);
		pol1.add( Box.createRigidArea(new Dimension(20,0)) );
		
		
		l2.setFont(new Font("Arial",Font.PLAIN,25));
		t2.setFont(new Font("Arial",Font.PLAIN,25));
		pol2.add( Box.createRigidArea(new Dimension(20,0)) );
		pol2.add( l2 );
		pol2.add( Box.createRigidArea(new Dimension(20,0)) );
		pol2.add( t2 );
		pol2.add( Box.createRigidArea(new Dimension(20,0)) );
		
		JPanel op = new JPanel();
	    op.setLayout(new BoxLayout(op, BoxLayout.X_AXIS)); 
		
		cb.setFont(new Font("Arial",Font.PLAIN,25));
		op.add( Box.createRigidArea(new Dimension(375,0)) );
		op.add(cb);
		op.add( Box.createRigidArea(new Dimension(400,0)) );
		
		JPanel buton = new JPanel();
	    buton.setLayout(new BoxLayout(buton, BoxLayout.X_AXIS)); 
		
		bt.setFont(new Font("Arial",Font.PLAIN,25));
		
		buton.add(bt);
	
		JPanel jos1 = new JPanel();
	    jos1.setLayout(new BoxLayout(jos1, BoxLayout.X_AXIS)); 
	    
	    JPanel jos2 = new JPanel();
	    jos2.setLayout(new BoxLayout(jos2, BoxLayout.X_AXIS)); 
	    
		
		rasp.setFont(new Font("Arial",Font.PLAIN,25));
		
		r1.setFont(new Font("Arial",Font.PLAIN,25));
		jos1.add( Box.createRigidArea(new Dimension(20,0)) );
		jos1.add( rasp );
		jos1.add( Box.createRigidArea(new Dimension(20,0)) );
		jos1.add(r1);
		jos1.add( Box.createRigidArea(new Dimension(20,0)) );
		
		
		rest.setFont(new Font("Arial",Font.PLAIN,25));
		
		r2.setFont(new Font("Arial",Font.PLAIN,25));
		jos2.add( Box.createRigidArea(new Dimension(20,0)) );
		jos2.add(rest );
		jos2.add( Box.createRigidArea(new Dimension(20,0)) );
		jos2.add( r2 );
		jos2.add( Box.createRigidArea(new Dimension(20,0)) );
		
		
		panel.add( Box.createRigidArea(new Dimension(0,25)) );
		panel.add(pol1);
		panel.add( Box.createRigidArea(new Dimension(0,20)) );
		panel.add(pol2);
		panel.add( Box.createRigidArea(new Dimension(0,25)) );
		panel.add(op);
		panel.add( Box.createRigidArea(new Dimension(0,20)) );
		panel.add(buton);
		panel.add( Box.createRigidArea(new Dimension(0,25)) );
		panel.add(jos1);
		panel.add( Box.createRigidArea(new Dimension(0,20)) );
		panel.add(jos2);
		panel.add( Box.createRigidArea(new Dimension(0,25)) );
		frame.add(panel);
		//Se deschide o ferestra care sfatuieste utilizatorul cum ar trebui sa introduca polinoamele
		JOptionPane.showMessageDialog(null, "Introduceti polinoamele fara saptii libere intre caractere respectand urmatorul format: 2x^2+5x-3");
		
		frame.setVisible(true);

	}
	// metodele pentru adaugarea de ActionListener la obiectele din interfata
	public void addP1Listener(ActionListener a) {
		t1.addActionListener(a);
	}
	
	public void addP2Listener(ActionListener a) {
		t2.addActionListener(a);
	}
	
	public void addButonListener(ActionListener a) {
		bt.addActionListener(a);
	}
	
	public void addBtListener(ActionListener a) {
		cb.addActionListener(a);
	}
	
	public String getStringP1() {
		return t1.getText();
	}
	
	public String getStringP2() {
		
		return t2.getText();
	}
	
	public void setStringRasp(String s) {
		r1.setText(s);
	}
	
	public void setStringRest(String s) {
		r2.setText(s);
	}
	
	public String getOperatie() {
		return (String) cb.getSelectedItem();
	}
	
}

