package PT2019.tema1.Tema1Project;

import java.util.ArrayList;

import junit.framework.TestCase;
import polinom.Monom;
import polinom.Polinom;

public class Junit_tema1 extends junit.framework.TestCase {

	public void testAdunare() {
	Polinom p1= new Polinom();
	Polinom p2= new Polinom();
	Polinom rez= new Polinom();
	
	p1=p1.transformare("x^3-2x-2");
	p2=p2.transformare("x-1");
	rez=p1.adunare(p2);
	System.out.println(p1.toString()+"+"+p2.toString()+"="+rez.toString());
	String asteptat="x^3-x-3";
	assertEquals(asteptat,rez.toString());
	}
	
	public void testScadere() {
		Polinom p1= new Polinom();
		Polinom p2= new Polinom();
		Polinom rez= new Polinom();
		
		p1=p1.transformare("x^3-2");
		p2=p2.transformare("2x^3-x");
		rez=p1.scadere(p2);
		System.out.println("("+p1.toString()+")-("+p2.toString()+")="+rez.toString());
		String asteptat="-x^3+x-2";
		assertEquals(asteptat,rez.toString());
		}
	
	public void testInmultire() {
		Polinom p1= new Polinom();
		Polinom p2= new Polinom();
		Polinom rez= new Polinom();
		
		p1=p1.transformare("x^2+7x+3");
		p2=p2.transformare("x^3+1");
		rez=p1.inmultire(p2);
		System.out.println("("+p1.toString()+")*("+p2.toString()+")="+rez.toString());
		String asteptat="x^5+7x^4+3x^3+x^2+7x+3";
		assertEquals(asteptat,rez.toString());
		}
	
	public void testImpartire() {
		Polinom p1= new Polinom();
		Polinom p2= new Polinom();
		ArrayList<Polinom> imp= new ArrayList<Polinom>();
		
		p1=p1.transformare("x^5-2x+1");
		p2=p2.transformare("x^2-1");
		imp=p1.impartire(p2);
		System.out.println("("+p1.toString()+")/("+p2.toString()+")="+imp.get(0).toString()+" rest"+imp.get(1).toString().toString());
		String asteptat1="x^3+x";
		String asteptat2="-x+1";
		assertEquals(asteptat1,imp.get(0).toString());
		assertEquals(asteptat2,imp.get(1).toString());
		}
	
	public void testderivareP1() {
		Polinom p1= new Polinom();
		Polinom rez= new Polinom();
		
		p1=p1.transformare("x^9-7x^3+1");
		rez=p1.derivare(p1);
		System.out.println("("+p1.toString()+")'="+rez.toString());
		String asteptat="9x^8-21x^2";
		assertEquals(asteptat,rez.toString());
		}
	
	public void testderivareP2() {
		Polinom p= new Polinom();
		Polinom rez= new Polinom();
		
		p=p.transformare("-x^8+10x^5-2x^3+x^2");
		rez=p.derivare(p);
		System.out.println("("+p.toString()+")'="+rez.toString());
		String asteptat="-8x^7+50x^4-6x^2+2x";
		assertEquals(asteptat,rez.toString());
		}
	
	public void testintegrareP1() {
		Polinom p= new Polinom();
		Polinom rez= new Polinom();
		
		p=p.transformare("12x^3-x^3+2");
		rez=p.integrare(p);
		System.out.println("S("+p.toString()+")dx="+rez.toString());
		String asteptat="3x^4-0.25x^4+2x";
		assertEquals(asteptat,rez.toString());
		}
	
	public void testintegrareP2() {
		Polinom p= new Polinom();
		Polinom rez= new Polinom();
		
		p=p.transformare("-20x^5+x^2+x");
		rez=p.integrare(p);
		System.out.println("S("+p.toString()+")dx="+rez.toString());
		String asteptat="-3.33x^6+0.33x^3+0.50x^2";
		assertEquals(asteptat,rez.toString());
		}
	
	
}
