# Sistem de procesare a polinoamelor #

Aplicatia este capabila de rezolvarea operatiilor de adunare, scadere, inmultire, impartire, derivare si integrare
asupra polinoamelor de o singura variabila si coeficienti intregi. Acestea sunt introduse de cate utilizator, 
coeficientii din monoame fiind de tip intreg in urma operatiilor de impartire si integrare, polinoamele rezultate in
urma calculelor vor avea coeficienti reali, care vor fi afisati cu 2 zecimale unde acestea sunt diferite de zero.

La pornirea aplicatiei se va deschide o fereastra care informeaza utilizatorul referitor la modul cum ar trebui introduse 
cele 2 seturi de date de intrare astfel incat programul sa functioneze in parametrii normali. Forma unui polinom are
urmatoarea structura: „2x^2+5x-3”. De asemena, datele pot fi introduse intr-o ordine aleatoare, nu este obligatoriu ca
monoamele sa fie ordonate descrescator, insa trebuie sa nu se repete monoame cu acelasi grad. 